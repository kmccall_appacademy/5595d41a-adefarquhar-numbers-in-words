class Fixnum
  def in_words
    raise 'Number is not an integer' unless is_a? Integer
    return 'zero' if zero?

    num_words = ''
    digits_left = self

    # for numbers greater than 100
    while digits_left >= 100
      digits_left, num_words = write_large_num(digits_left, num_words)
    end

    # for numbers greater than 9, less than 100
    digits_left, num_words = write_big_num(digits_left, num_words)

    # for numbers single digits
    digits_left, num_words = write_singles(digits_left, num_words)

    num_words
  end

  def write_singles(digits_left, num_words)
    singles = { 1=>'one', 2=>'two', 3=>'three', 4=> 'four',
                5=> 'five', 6=> 'six', 7=> 'seven', 8=> 'eight',
                9=> 'nine' }

    write_digit = digits_left
    digits_left = 0
    num_words += singles[write_digit] if write_digit > 0

    return digits_left, num_words
  end

  def write_big_num(digits_left, num_words)
    tens = { 1=> 'ten', 2=> 'twenty', 3=> 'thirty', 4=> 'forty',
             5=> 'fifty', 6=> 'sixty', 7=> 'seventy', 8=> 'eighty',
             9=> 'ninety'}
    teens = { 1=> 'eleven', 2=> 'twelve', 3=> 'thirteen',
              4=> 'fourteen', 5=> 'fifteen', 6=>'sixteen',
              7=>'seventeen', 8=> 'eighteen', 9=> 'nineteen' }

    write_digit = digits_left/10
    digits_left -= write_digit*10
    if write_digit > 0
      if write_digit == 1 && digits_left > 0
        num_words += teens[digits_left]
        digits_left = 0
      else
        num_words += tens[write_digit]
      end

      num_words += ' ' if digits_left > 0
    end

    return digits_left, num_words
  end

  def write_large_num(digits_left, num_words)
    div, range = find_range(digits_left)

    write_digit = digits_left / div
    digits_left -= write_digit * div

    if write_digit > 0
      digit = write_digit.in_words
      num_words += "#{digit} #{range}"
      num_words += ' ' if digits_left > 0
    end

    return digits_left, num_words
  end

  def find_range(number)
    if number / 1_000_000_000_000 > 0
      return 1_000_000_000_000, 'trillion'
    elsif number / 1_000_000_000 > 0
      return 1_000_000_000, 'billion'
    elsif number / 1_000_000 > 0
      return 1_000_000, 'million'
    elsif number / 1_000 > 0
      return 1_000, 'thousand'
    elsif number / 100 > 0
      return 100, 'hundred'
    end
  end
end
